Hjelp, jeg designer for THE WORLD WIDE WEB
(Do's and dont's for webdesignere)

Sist oppdatert 04.10.13

**GENERELT**

1. Vær ryddig og beskrivende

Ti tusen PSD-lag uten navn er ganske slitsomt å nøste opp i. Prøv å gi alle lag beskrivende navn, og å dele opp ting i mapper så mye som mulig.

Prøv også gjerne å følge sidens kronologi så langt det er mulig – dersom jeg f.eks. leter etter footeren forventer jeg å finne en mappe som heter "Footer" e.l. nederst i PSD'en, ikke øverst eller i midten.

2. Inkludér fonter i leveranse

Desktopversjoner av custom fonter bør inkluderes i leveranse, slik at utvikler kan installere disse og være sikker på at han/hun ser det designeren ser i Photoshop.

3. Len deg gjerne på rammeverket

Som regel benyttes det et rammeverk for frontendutviklingen - f.eks. Foundation, Bootstrap e.l..

Sett deg inn i hva det aktuelle rammeverket inneholder - det er ingen skam å bruke "ferdigelementer" som f.eks. knapper, skjemaelementer, mobilmenyer etc. så lenge det passer inn i designet, og det sparer masse tid.


**GRIDS**

1. Følg griddet

Sett deg inn i hvilket gridsystem som benyttes – f.eks. Foundation 4 med 12 kolonner – og pass på at alle hovedelementer (f.eks. content/sidebar) følger dette systemet. 

Dersom du bruker et grid overlay i Photoshop (eller setter opp smart guides som stemmer med systemet) trenger du ikke tenke noe mer på dette, alternativt kan du bruke enkel matte for å finne de ulike breddene:

For et 12-kolonners grid vil hver kolonne være 8.3333% bred. Skal du designe et element som bruker halve bredden, altså 5 kolonner, blir utregningen slik:

(bredde på container) / (0.083333 * 5).

2. Bruk et grid overlay

Inkludér et halvveis gjennomsikt grid-lag øverst i PSD'en – så blir det både enklere å designe innenfor griddet, og enklere for utvikler å sjekke hvor mange kolonner et element skal dekke.

Alternativt kan du sette opp smart guides.

Husk å ta høyde for gutters (mellomrom mellom kolonnene). I Foundation er standard gutters 30px brede, dvs. at hver kolonne har 15px padding på hver side.


**ANIMASJONSDESIGN, OPPFØRSEL OG STATES**

1. Husk å designe states

Alle interaktive elementer - f.eks. knapper og lenker - bør ha en hover og/eller aktiv state designet opp. Det er glimrende dersom f.eks. en knapp settes opp i en folderstruktur hvor de ulike tilstandene har hver sin beskrivende undermappe:

[knapp] (PSD-mappe)
	[default]
  [mouseover]
	[disabled]
	[active]

Tips: Fireworks er et drøyt mye bedre verktøy enn Photoshop for webdesign, og har f.eks. en egen objekttype for "symbol", hvor ulike states enkelt kan defineres og designes opp.

Enklere elementer som f.eks. hover-farge på lenker kan selvfølgelig bare eksemplifiseres i dummyinnhold.

2. Utvikle et animasjonskonsept

Animasjoner er vanskelig å illustrere i Photoshop, og bør beskrives tekstuelt - enten med tekstbokser i PSD'en, eller i et følgedokument, gjerne med ting som piler etc.

Har du en mer generell visjon om hvordan siden skal oppføre seg mtp. bevegelse, bør det utarbeides et såkalt animasjonsdesign – dette er enklest sagt et sett med regler for bevegelse, f.eks. "horisontale overganger benytter 25% anticipation", "alle fades benytter 50% ease in" etc.

3. Kommunisér spesiell oppførsel

Dersom det er ytterligere, visuell logikk som ikke følger naturlig av det flate designet, bør dette beskrives på samme måte som animasjonene. Eksempler på dette kan være et skjema som endrer seg underveis i utfyllingen, elementer som skal inkluderes via AJAX etc, sticky menus etc.


**FONTER**

1. Vær restriktiv og konsekvent med fontstørrelser

Brukeren ser neppe forskjell på en header satt i 28px og en header satt i 29px – så vurdér om du kan sette begge i en av størrelsene.

Optimalt sett har alle header-tags (h1, h2, h3 osv) samme størrelse gjennom hele designet; dvs. at en h1 f.eks. alltid er 32px, en h2 alltid 28px osv.

2. Vær restriktiv med fonter og vekter

Tommelfingerregelen er at hver vekt (f.eks. italic eller bold) legger til mellom 20 og 50kb på pageload. Kjører man 3-4 font familier med 3-5 vekter hver (regular, italic, bold, bold italic, black) kryper filtyngden raskt opp til 500kb-1mb, noe som gjør det vanskelig å lage en spiffy løsning.

Dersom du har ett enkelt element på siden (f.eks. et sitat) som er satt i en vekt eller en familie som ikke brukes noe annet sted, vurdér om det er mulig å benytte en av familiene eller vektene som allerede er i bruk.

Forøvrig er det svært sjelden at et design har godt av mer enn 2-3 familier på samme side.

3. Vær konsekvent med linjeavstander

Linjeavstand bør følge fontstørrelse. I tillegg bør elementer som har identisk familie, vekt og størrelse også ha identisk linjeavstand.

4. Bruk hele verdier

Rund fontstørrelser av til hele pixel-verdier, f.eks. 16px, ikke 15.6px.


**FARGER**

1. Vær konsekvent og restriktiv med farger

Unngå farger som er *nesten* like – brukeren ser neppe forskjell på en #F1F1F1 og en #F2F2F2, så det er ingen grunn til å bruke begge.

Utvikleren setter opp alle farger i designet som variabler, og får en mye enklere jobb dersom han/hun kan forholde seg til "grå" og "lysegrå", og ikke "grå", "lysegrå", "bitte-litte-granne-lysere-grå" og "enda-bitte-litte-granne-lysere-grå" – der de tre siste valørene i praksis er identiske.

Dersom det produseres et fargekart, vil man raskt kunne fange opp evt. farger som i praksis er identiske. Da kan man vurdere å "slå sammen" disse til én hexkode.

2. Unngå å bruke gjennomsiktighet og blend modes

Blend modes er umulig å overføre til CSS, og er ikke en OK måte å generere en farge på.

Opacity kan benyttes, men er kludrete i eldre nettlesere og bør brukes med måte – aldri for å få frem en spesifikk valør på solide blokker, f.eks.

Layer styles som f.eks. color overlay er greit.

Graderinger bør brukes med måte, og helst ikke i sammenhenger hvor det er animasjon.


**GRAFIKK**

1. Bruk sømløse tiles for tekstur og mønstere

I de aller fleste tilfeller skal teksturer og mønstere repeteres i bredde og/eller høyde, for å dekke hele bakgrunnen av et element.

Det er viktig at grafikken som leveres utvikler lar seg repetere sømløst i bredde og høyde.

2. Gjør all grafikk tilgjengelig i Retinaoppløsning

All grafikk som f.eks. ikoner, teksturer etc. må være tilgjengelig i minst dobbelt oppløsning. For ting som opprinnelig er vektorgrafikk er det enklest og best at dette ikke rastreres, slik at utvikler selv kan eksportere de oppløsningene som er nødvendige, eller konvertere til SVG.

For rastergrafikk er det et hett tips å konvertere dette til et Smart Object med dobbel oppløsning, før elementet tas ned i størrelse i PSD'en.


**NYTTIGE RESSURSER**

http://photoshopetiquette.com/
http://webdosanddonts.com/



